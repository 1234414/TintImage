package com.zoke.tint;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.zoke.tint.helper.SystemBarTintManager;

import org.xutils.x;

/**
 * @author 大熊 QQ:651319154
 */
public class BaseActivity extends AppCompatActivity {

    private RelativeLayout mStatusBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        x.view().inject(this);
        //为了4.1兼容沉浸式
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            initStatusBar();
            //提供设置占位显示或者隐藏的方法 暴漏给子类进行设置
            if (mStatusBar != null) {
                //只有这个地方需要设置宽高
                mStatusBar.setVisibility(View.VISIBLE);
            }
        } else {
            if (mStatusBar != null) {
                //设置隐藏
                mStatusBar.setVisibility(View.GONE);
            }
        }
    }

    protected void initStatusBar() {
        //需要获取状态栏的高度
        setTranslucentStatus(true);
        SystemBarTintManager mTintManager = new SystemBarTintManager(this);
        mTintManager.setStatusBarTintEnabled(true);
        mTintManager.setStatusBarAlpha(0.0f);
        mTintManager.setNavigationBarTintEnabled(false);
        //设置底部导航栏的颜色 -- 透明色
        mTintManager.setNavigationBarTintColor(R.color.colorPrimaryDark);
        //设置状态栏颜色
        mTintManager.setStatusBarTintColor(R.color.colorPrimaryDark);
        mTintManager.setTintColor(R.color.colorPrimaryDark);
    }


    private void setTranslucentStatus(boolean on) {
        Window win = getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        final int bits = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }


    //返回当前的StatusBar--动态显示和隐藏这个View
    protected RelativeLayout getStatusBar() {
        if (mStatusBar == null)
            mStatusBar = (RelativeLayout) findViewById(R.id.status_bar);
        return mStatusBar;
    }
}
