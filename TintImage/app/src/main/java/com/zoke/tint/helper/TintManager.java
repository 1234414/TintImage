package com.zoke.tint.helper;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.widget.ImageView;

/**
 * @author 大熊 QQ:651319154
 */
public class TintManager {
    /**
     * 实现ImageView的变色
     *
     * @param imageView
     * @param color
     */
    public static void setTint(@NonNull ImageView imageView, @ColorInt int color) {
        ColorStateList s1 = ColorStateList.valueOf(color);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            imageView.setImageTintList(s1);
        } else {
            PorterDuff.Mode mode = PorterDuff.Mode.SRC_IN;
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
                mode = PorterDuff.Mode.MULTIPLY;
            }
            if (imageView.getDrawable() != null)
                imageView.getDrawable().setColorFilter(color, mode);
        }
    }
}
