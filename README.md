#效果图

![输入图片说明](http://git.oschina.net/uploads/images/2016/0421/191445_835e5773_312885.jpeg "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0421/191455_5fae664c_312885.jpeg "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0421/191506_e788f35d_312885.jpeg "在这里输入图片标题")

#核心代码
```
/**
     * 实现ImageView的变色
     *
     * @param imageView
     * @param color
     */
    public static void setTint(@NonNull ImageView imageView, @ColorInt int color) {
        ColorStateList s1 = ColorStateList.valueOf(color);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            imageView.setImageTintList(s1);
        } else {
            PorterDuff.Mode mode = PorterDuff.Mode.SRC_IN;
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
                mode = PorterDuff.Mode.MULTIPLY;
            }
            if (imageView.getDrawable() != null)
                imageView.getDrawable().setColorFilter(color, mode);
        }
    }
```